import {configure} from '@storybook/react';

const req = require.context('../src/', true, /stories\.jsx$/);
function loadStories() {
  req.keys().forEach(req)
}

require('../src/styles/styles.css');

configure(loadStories, module);

