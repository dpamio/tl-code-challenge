# Third Love Code Challenge

## Deployment

The build of the code challenge is deployed and available @ [dpamio's bitbucket pages](https://dpamio.bitbucket.io/) (https://dpamio.bitbucket.io/).

## Desicions and fundamentations

* **[react]** Started development using [Create React App](https://github.com/facebookincubator/create-react-app) to make scaffolding quick.
* **[git]** Used the standard git commit messages guidelines from [Stephen Parish](https://gist.github.com/stephenparish/9941e89d80e2bc58a153), plus the [gitemoji](https://gitmoji.carloscuesta.me) convention to easily identify commit types with emojis.
* **[css]** Decided to use floats instead of flexbox for responsive columns to prevent some nasty bugs in IE11. Also, due to IE11 akward implementation of css grid and the fact that cannot be pollifilled with cssnext, I’m forced not to use css grid.
* **[storybook]** used [Storybook](https://storybook.js.org) to manage the small UI library this project will have and to play around with the components while I develop them.
* **[dev-process]** Contra-nature dev flow: Instead of making evolutive development, which would make the first version and unstyled, text based, non-layouted but functional one, I decided to just go ahead with full development starting with building blocks (unusable per-se) and consider the whole development “done” when I finish the whole thing, even though I will make iterative development of components and get feedback from them. This is mainly due to: 1) The small size of the development. 2) It’s more a visual-complex task rather than a functionality-complex one.
* **[css]** decided to use 7-1 pattern with BEM for naming classes.
* **[css]** to test the PerfectPixel, opted to use Perfecct Pixel chrome extension by WellDoneCode to aid the fine tuning.
* **[react]** used fetch as the request library mainly due to: 1) its simple for a code challenge with just one static GET endpoint. 2) It’s polyfilled by react-create-app for IE11.
* **[react]** used [react-image-gallery](https://www.npmjs.com/package/react-image-gallery) for the image carousel.
* **[lint]** initally used the recommended eslint rules from react-create-app, but finally switched to airbnb linter with support for react.
* **[css]** initially tried no-preprocessor, CSS Module + PostCSS/nextcss, but ended up being too hacky for the seed used and akward. Finally switched to old-school SASS with [node-sass-chokidar](https://www.npmjs.com/package/node-sass-chokidar). That way the full SCSS structure is separated from the js and can be reused in other platforms.


## Deviations from the requirements
1. I’ve created a new endpoint with more variability in some fields (i.e.: price) to showcase more scenarios. You can see it here: http://www.mocky.io/v2/5b4e202f3200004d009c2942
2. Even though I’ve used plain CSS (media-query) for most of the responsive design, I’ve used [react-responsive](https://github.com/contra/react-responsive) for some responsive design (like determining responsive props in the third party gallery component).
3. I had to implement some intermediate breakpoints (lg-1024) with some intermediate dimensions for some components to make the full smooth transition from phone to xl desktop.

### Additional functionality implemented

There were a handful of cases that were either not included or too easy to implement (low hanging fruits) which I've implemented:

#### Feature: Keep selected band/cup size if still valid

Scenarios:
```gherkin

Given selected band size === 32
And selected cup size === F
And available variants include 32F, 34F
When band size 34 is selected
Then cup size F remains selected

Given selected band size === 32
And selected cup size === F
And available variants ONLY includes 32F, 34E
When band size 34 is selected
Then cup size becomes undefined

```

#### Feature: Autoselect when there is only one option

Scenarios:
```gherkin

Given the selected color is naked-1
And the selected band size is 32
And the ONLY available value for band size for color naked-2 is 38
When the color naked-2 is selected
Then the value 38 for band size is automatically selected

Given the selected color is naked-1
And the selected band size is 32
And the ONLY available value for band size for color naked-2 is 38
And the ONLY available value for cup size for naked-2 38 is B
When the color naked-2 is selected
Then the value 38 for band size is automatically selected
And the value B for cup size is automaticalluy selected
```

#### Feature: Display stock for a set of variants

Scenarios:
```gherkin

Given there are different stock levels for variants in the naked-1 color
When the naked-1 color is selected
Then the stock value displayed is the range of stock levels for the available variants with the format "<min stock>-<max stock>"

Given color naked-1 is selected
And there are different stock levels for variants in the naked-1 band size 32
When the band size 32 is selected
Then the stock value displayed is the range of stock levels for all the cup sizes of that color/band size with the format "<min stock>-<max stock>"

Given color naked-1 is selected
And there are different stock levels for variants in the naked-1 cup size B
When the cup size B is selected
Then the stock value displayed is the range of stock levels for all the band sizes of that color/cup size with the format "<min stock>-<max stock>"

```

#### Feature: Display price for a set of variants

Scenarios:
```gherkin

Given there are different prices for variants in the naked-1 color
When the naked-1 color is selected
Then the price value displayed is the range of prices for the available variants with the format "<min price>-<max price>"

Given color naked-1 is selected
And there are different prices for variants in the naked-1 band size 32
When the band size 32 is selected
Then the price value displayed is the range of prices for all the cup sizes of that color/band size with the format "<min price>-<max price>"

Given color naked-1 is selected
And there are different prices for variants in the naked-1 cup size B
When the cup size B is selected
Then the price value displayed is the range of prices for all the band sizes of that color/cup size with the format "<min price>-<max price>"

```


## Outscoped

There are several things I haven’t included in the final code due to different reasons, mainly, lack of additional time.

* [fixed] ~~Smells left in the code: updating the status in didComponentUpdate()~~
* Duplicated breakpoint magic numbers between React and Sass variables.
* No-Redux: even though I’m a redux lover, the size of the challenge and the time available wasn’t adequate for over engineering the state management (from my point of view).
* No-flow: opted for just using propTypes.
* Proper animations and style tweaks (like making the `“V”` to transform into a `“^”` when a select is opened.


## Oddities:
* As the code challenge is just to display one product, nowhere in the fetch calls there is any “ID” or query being sent to that endpoint.

## Perfect Pixel

Here you can see the outcome of the pixel perfect test with the chrome plugin:

### SM

You can notice some not-perfect pixels on the cup size select drop down 'V' symbol, which is derived from some inconsistence between the two selects in the zeplin wireframes.

![Pixel Perfect on XS/SM](https://bitbucket.org/dpamio/tl-code-challenge/raw/82436be83ecd26663e56eb17b037217079467d8f/md_images/pp_sm.gif)

### XL

I couldn't include bullets Pixel Perfect test in the animated gif, but I've tested them and they correspond to the wireframes.

![Pixel Perfect on XL](https://bitbucket.org/dpamio/tl-code-challenge/raw/dc0d1977635a082ad790a01f48f6575c15b0187f/md_images/pp_xl.gif)





