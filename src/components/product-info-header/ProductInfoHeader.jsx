import React from 'react';
import { PropTypes } from 'prop-types';

const ProductInfoHeader = (props) => {
  const { productName, price } = props;
  return (
    <header className="ProductInfoHeader">
      <h1 className="ProductInfoHeader__Title">
        {productName}
      </h1>
      <h2 className="ProductInfoHeader__Subtitle">
        {price}
      </h2>
    </header>
  );
};

ProductInfoHeader.defaultProps = {
  productName: '',
  price: '',
};

ProductInfoHeader.propTypes = {
  productName: PropTypes.string,
  price: PropTypes.string,
};

export default ProductInfoHeader;
