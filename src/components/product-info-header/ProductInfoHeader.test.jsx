import React from 'react';
import { render, cleanup } from 'react-testing-library';
import ProductInfoHeader from './ProductInfoHeader';

afterEach(cleanup);

fit('Calls the callback', () => {
  const { getByText } = render(<ProductInfoHeader productName="prod name" price="$50-$70" />);

  expect(getByText('prod name')).toBeVisible();
  expect(getByText('$50-$70')).toBeVisible();
});
