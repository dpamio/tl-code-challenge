import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';
import ColorButton from './ColorButton';

afterEach(cleanup);

it('Calls the callback', () => {
  const spy = jest.fn();
  const { getByTestId } = render(<ColorButton onColorSelected={spy} colorId="naked-1" isSelected />);

  fireEvent(
    getByTestId('color selection button'),
    new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
    }),
  );

  expect(spy).toHaveBeenCalledTimes(1);
});
