import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';

import ColorButton from './ColorButton';

// const stories = storiesOf('Color Selection Button', module).addDecorator(withInfo);
const stories = storiesOf('Color Selection Button', module);

const colors = [
  { colorId: 'naked-1', color: '#ffefe5' },
  { colorId: 'naked-1', color: '#ffefe5' },
  { colorId: 'naked-2', color: '#dab5a1' },
  { colorId: 'naked-3', color: '#d49d89' },
  { colorId: 'naked-4', color: '#6a3a33' },
  { colorId: 'naked-5', color: '#524346' },
  { colorId: 'Not Specified' },
];

const buttons = selected => colors.map(colorMetadata => (
  <div style={{ margin: '10px' }}>
    <span>
Color:
      {colorMetadata.colorId}
    </span>
    {' '}
    <ColorButton colorId={colorMetadata.colorId} isSelected={selected} onColorSelected={action('color-selected')} colorMetadata={colorMetadata} />
  </div>
));


stories.add('Unselected', withInfo('')(() => (
  <div>
    {' '}
    {buttons(false)}
    {' '}
  </div>
)));

stories.add('Selected', withInfo('')(() => (
  <div>
    {' '}
    {buttons(true)}
    {' '}
  </div>
)));
