import React from 'react';
import { PropTypes } from 'prop-types';

const ColorButton = (props) => {
  const {
    colorMetadata, isSelected, onColorSelected, colorId,
  } = props;
  const style = {
    backgroundColor: colorMetadata.color,
  };

  return (
    <button
      data-testid="color selection button"
      type="button"
      className={`ColorButton ${isSelected ? 'ColorButton--selected' : ''}`}
      onClick={() => onColorSelected(colorId)}
      style={style}
    />
  );
};

ColorButton.defaultProps = {
  colorMetadata: {
    color: 'gray',
  },
  isSelected: false,
  onColorSelected: () => {},
};

ColorButton.propTypes = {
  colorMetadata: PropTypes.shape({
    color: PropTypes.string,
  }),
  isSelected: PropTypes.bool,
  onColorSelected: PropTypes.func,
  colorId: PropTypes.string.isRequired,
};

export default ColorButton;
