import React, { Component } from 'react';
import Select from 'react-select';
import { PropTypes } from 'prop-types';
import ColorSelector from './color-button/ColorButton';
import { convertToSelectOptionObject } from '../../util/dom-utils';

const NOT_FOUND_IN_ARRAY = -1;

/**
 * Returns a function capable of filtering colors over a set of variants
 * @param color is the colorId to filter by (e.g.: naked-1)
 * @returns {function(T)} the reducer function.
 */
const sameColor = color => variant => (variant.colorName === color || color === undefined);

/**
 * Returns a function capable of filtering a band size over a set of variants
 * @param bandSize is the band size to look for (e.g.: 32)
 * @returns {function(T)} the reducer function.
 */
const sameBandSize = bandSize => variant => (
  variant.bandSize === bandSize || bandSize === undefined
);

// ToDo: Extract it to some utils/reduce.js function utility module.
/**
 * Returns a reducer function that can extract elements with different fieldName values in an array.
 * @param fieldName is the fieldName to reduce over.
 * @returns {function(T)} the reducer function.
 */
const distinct = fieldName => (allOutputs, currentObject) => {
  if (allOutputs.indexOf(currentObject[fieldName]) === NOT_FOUND_IN_ARRAY) {
    allOutputs.push(currentObject[fieldName]);
  }
  return allOutputs;
};

/**
 * Returns a down arrow compliant with the wireframe assets provided.
 */
const arrowRenderer = () => (
  <span>
    <img src="/images/arrow-small-dark.svg" alt="Arrow to display drop down menu" />
  </span>
);

class VariantSelector extends Component {
  constructor() {
    super();
    this.state = {
      selectedColor: undefined,
      selectedBandSize: undefined,
      selectedCupSize: undefined,
    };
  }


  /**
   * Notifies the variant(s) that matches the current state.
   */
  componentDidUpdate() {
    const { onVariantsSelected } = this.props;

    onVariantsSelected(this.matchExactVariant() || this.matchVariants());
  }

  /**
   * Handles the change in the color button array. updates the state accordingly.
   * @param colorId is the color "id" (e.g.: 'naked-1')
   */
  onColorSelected(colorId) {
    this.setState({
      selectedColor: colorId,
    });
  }

  /**
   * Handles the change in the band size select. updates the state accordingly.
   * @param bandSize is the band size (e.g.: '32')
   */
  onBandSizeSelected(bandSize) {
    this.setState({
      selectedBandSize: bandSize,
    });
  }

  /**
   * Handles the change in the cup size select. updates the state accordingly.
   * @param cupSize is the cup size (e.g.: 'B')
   */
  onCupSizeSelected(cupSize) {
    this.setState({
      selectedCupSize: cupSize,
    });
  }


  /**
   * It generates a representation of stock range over the list of matching variants.
   * If the max == to the min value, then just that value gets returned, otherwise a string
   * containing "<min>-<max>" is returned insted.
   * @returns {string} either the unique or the two-number range.
   */
  getStockRange() {
    const sortedAvailableStockNumbers = this.matchVariants().map(
      variant => (parseInt(variant.stock, 10)),
    ).sort();

    if (sortedAvailableStockNumbers[0]
      === sortedAvailableStockNumbers[sortedAvailableStockNumbers.length - 1]) {
      return sortedAvailableStockNumbers[0];
    }
    return `${sortedAvailableStockNumbers[0]}-${sortedAvailableStockNumbers[sortedAvailableStockNumbers.length - 1]}`;
  }

  /**
   * Calculates the current color based on: 1) The color manually selected, 2) the default color.
   * If no manually selected color, then the default color from the defaultColor prop is returned
   * and if that one isn't defined either, just the first color.
   * @returns {string} the current colorId (e.g.: naked-1)
   */
  getCurrentColor() {
    const { selectedColor } = this.state;
    const { defaultColor, colors } = this.props;
    return selectedColor || defaultColor || colors[0];
  }

  /**
   * Calculates the current band size based on 1) the band size manually selected 2) the available
   * band sizes.
   * If there is only one available band size, that's the one returned, otherwise, if there is one
   * manually selected, that's the returned one, unless the manually selected is not inside the
   * array of the available ones, in which case undefined is returned.
   * @returns {string} the band size id (e.g.: "32")
   */
  getCurrentBandSize() {
    const { selectedBandSize } = this.state;
    const bandSizes = this.availableBandSizes();

    if (bandSizes.length === 1) {
      return bandSizes[0];
    }

    if (bandSizes.indexOf(selectedBandSize) !== NOT_FOUND_IN_ARRAY) {
      return selectedBandSize;
    }

    return undefined;
  }

  /**
   * Calculates the current cup size based on 1) the cup size manually selected 2) the available
   * cup sizes.
   * If there is only one available cup size, that's the one returned, otherwise, if there is one
   * manually selected, that's the returned one, unless the manually selected is not inside the
   * array of the available ones, in which case undefined is returned.
   * @returns {string} the cup size id (e.g.: "B")
   */
  getCurrentCupSize() {
    const { selectedCupSize } = this.state;
    const cupSizes = this.availableCupSizes();

    if (cupSizes.length === 1) {
      return cupSizes[0];
    }

    if (cupSizes.indexOf(selectedCupSize) !== NOT_FOUND_IN_ARRAY) {
      return selectedCupSize;
    }

    return undefined;
  }


  /**
   * Matches all the variants that satisfy equality to the defined components. This means that,
   * if one of those elements is undefined, it will broaden the set of matching variants to all
   * of them. e.g.: undefined band size will get all the variants of matching color/cup sizes.
   * @returns {[]} the list of matched variants.
   */
  matchVariants() {
    const { variants } = this.props;
    const currentColor = this.getCurrentColor();
    const currentBandSize = this.getCurrentBandSize();
    const currentCupSize = this.getCurrentCupSize();

    return variants.filter(
      variant => (variant.colorName === currentColor || currentColor === undefined)
      && (variant.bandSize === currentBandSize || currentBandSize === undefined)
      && (variant.cupSize === currentCupSize || currentCupSize === undefined),
    );
  }


  // ToDo: see if these next three functions can be more "pure" not using this.<anything>.
  /**
   * Matches a unique variant with the values of the components (color/band size/ cup size).
   * This means that if one of those elements is undefined, it will find no variant matching.
   * @returns {T} the variant which exatly matches the three values.
   */
  matchExactVariant() {
    const { variants } = this.props;
    const currentColor = this.getCurrentColor();
    const currentBandSize = this.getCurrentBandSize();
    const currentCupSize = this.getCurrentCupSize();

    return variants.find(variant => variant.colorName === currentColor
      && variant.bandSize === currentBandSize
      && variant.cupSize === currentCupSize);
  }


  /**
   * Calculates the available band sizes based on the available variants and the selected color.
   * @returns {Array<string>} is the list of unique band sizes, sorted by number.
   */
  availableBandSizes() {
    const { variants } = this.props;
    const currentColor = this.getCurrentColor();

    return variants.filter(sameColor(currentColor))
      .reduce(distinct('bandSize'), [])
      .sort(); // ToDo: default sort works with band size typical values by cahnce.
  }

  /**
   * Calculates the available cup sizes based on the available variants, the selected color and the
   * selected band size.
   * @returns {Array<string>} is the list of unique cup sizes, sorted by number.
   */
  availableCupSizes() {
    const { variants } = this.props;
    const currentBandSize = this.getCurrentBandSize();

    const currentColor = this.getCurrentColor();
    return variants.filter(sameColor(currentColor))
      .filter(sameBandSize(currentBandSize))
      .reduce(distinct('cupSize'), [])
      .sort(); // ToDo: default sort works with band size typical values by cahnce.
  }

  render() {
    const { colorsMetadata, colors } = this.props;

    const currentColor = this.getCurrentColor();
    const colorButtons = colors.map(color => (
      <ColorSelector
        key={color}
        colorId={color}
        colorMetadata={colorsMetadata[color]}
        isSelected={color === currentColor}
        onColorSelected={(col) => { this.onColorSelected(col); }}
      />
    ));

    return (
      <main className="VariantSelector">
        <section className="VariantSelector__ColorsSection">
          <div className="VariantSelector__SelectedColor">
            <label className="VariantSelector__SelectedColorLabel" htmlFor="selected-color">
              Color:
              {' '}
            </label>
            <span className="VariantSelector__SelectedColorValue" id="selected-color">
              {colorsMetadata[currentColor] ? colorsMetadata[currentColor].name : ''}
            </span>
          </div>
          <ul className="VariantSelector__ColorsList">
            {colorButtons}
          </ul>
        </section>
        <section className="VariantSelector__Stock">
          <label htmlFor="stock" className="VariantSelector__StockLabel">
            Stock:
            {' '}
          </label>
          <span id="stock" className="VariantSelector__StockValue">
            {this.getStockRange()}
          </span>
        </section>
        <section className="VariantSelector__Sizes">
          <section className="VariantSelector__BandSize">
            <label htmlFor="band-size-select" className="VariantSelector__BandSizeLabel">
              Band Size
            </label>
            <Select
              id="band-size-select"
              className="VariantSelector__BandSizeSelect"
              searchable={false}
              arrowRenderer={arrowRenderer}
              options={this.availableBandSizes().map(convertToSelectOptionObject)}
              simpleValue
              multi={false}
              value={this.getCurrentBandSize()}
              clearable={false}
              onChange={bandSize => this.onBandSizeSelected(bandSize)}
              placeholder="Select"
            />
          </section>
          <section className="VariantSelector__CupSize">
            <label htmlFor="cup-size-select" className="VariantSelector__CupSizeLabel">
              Cup Size
            </label>
            <Select
              id="cup-size-select"
              className="VariantSelector__CupSizeSelect"
              searchable={false}
              arrowRenderer={arrowRenderer}
              options={this.availableCupSizes().map(convertToSelectOptionObject)}
              simpleValue
              multi={false}
              value={this.getCurrentCupSize()}
              clearable={false}
              onChange={cupSize => this.onCupSizeSelected(cupSize)}
              placeholder="Select"
            />
          </section>
        </section>
      </main>
    );
  }
}

VariantSelector.defaultProps = {
  defaultColor: undefined,
  onVariantsSelected: () => {},
  variants: [],
  colorsMetadata: {},
  colors: [],
};

VariantSelector.propTypes = {
  defaultColor: PropTypes.string,
  onVariantsSelected: PropTypes.func,
  variants: PropTypes.arrayOf(PropTypes.any),
  // eslint-disable-next-line react/forbid-prop-types
  colorsMetadata: PropTypes.object,
  colors: PropTypes.arrayOf(PropTypes.any),
};

export default VariantSelector;
