/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import VariantSelector from './VariantSelector';

const stories = storiesOf('Variant Selector', module);
const colors = ['naked-1', 'naked-2', 'naked-3', 'naked-4'];
const colorsMetadata = {
  'naked-1': {
    name: 'Naked 1',
    color: '#ffefe5',
  },
  'naked-2': {
    name: 'Naked 2',
    color: '#dab5a1',
  },

  'naked-3': {
    name: 'Naked 3',
    color: '#d49d89',
  },

  'naked-4': {
    name: 'Naked 4',
    color: '#6a3a33',
  },
  'naked-5': {
    name: 'Naked 5',
    color: '#524346',
  },
};

const variantsData = [{
  id: 6989569458233,
  price: '69.00',
  colorName: 'naked-1',
  bandSize: '32',
  cupSize: 'E',
  braSize: '32E',
  stock: 764,
}, {
  id: 6989569491001,
  price: '65.00',
  colorName: 'naked-1',
  bandSize: '32',
  cupSize: 'F',
  braSize: '32F',
  stock: 158,
}, {
  id: 6989722583097,
  price: '68.00',
  colorName: 'naked-2',
  bandSize: '32',
  cupSize: 'E',
  braSize: '32E',
  stock: 1109,
}, {
  id: 6989722615865,
  price: '68.00',
  colorName: 'naked-2',
  bandSize: '32',
  cupSize: 'F',
  braSize: '32F',
  stock: 1109,
}, {
  id: 4615861469221,
  price: '68.00',
  colorName: 'naked-2',
  bandSize: '34',
  cupSize: 'D',
  braSize: '34D',
  stock: 1797,
}, {
  id: 6989722648633,
  price: '68.00',
  colorName: 'naked-2',
  bandSize: '34',
  cupSize: 'F',
  braSize: '34F',
  stock: 100,
}, {
  id: 6989459193913,
  price: '68.00',
  colorName: 'naked-3',
  bandSize: '32',
  cupSize: 'E',
  braSize: '32E',
  stock: 300,
}, {
  id: 6989459226681,
  price: '68',
  colorName: 'naked-3',
  bandSize: '32',
  cupSize: 'F',
  braSize: '32F',
  stock: 320,
}, {
  id: 6989459292217,
  price: '68.99',
  colorName: 'naked-3',
  bandSize: '34',
  cupSize: 'F',
  braSize: '34F',
  stock: 264,
}, {
  id: 4615725842469,
  price: '70.00',
  colorName: 'naked-4',
  bandSize: '32',
  cupSize: 'E',
  braSize: '32E',
  stock: 214,
}, {
  id: 4615725908005,
  price: '68.00',
  colorName: 'naked-4',
  bandSize: '34',
  cupSize: 'D',
  braSize: '34D',
  stock: 133,
}, {
  id: 4615725973541,
  price: '120.00',
  colorName: 'naked-4',
  bandSize: '34',
  cupSize: 'F',
  braSize: '34F',
  stock: 891,
}, {
  id: 6989673398329,
  price: '68.00',
  colorName: 'naked-5',
  bandSize: '32',
  cupSize: 'E(DD)',
  braSize: '32E(DD)',
  stock: 98,
}, {
  id: 6989673431097,
  price: '68.00',
  colorName: 'naked-5',
  bandSize: '32',
  cupSize: 'F',
  braSize: '32F',
  stock: 98,
}, {
  id: 6989673496633,
  price: '68.00',
  colorName: 'naked-5',
  bandSize: '34',
  cupSize: 'E',
  braSize: '34E',
  stock: 348,
}];

const defaultColor = 'naked-2';

stories.add('Normal', () => (
  <VariantSelector
    onVariantsSelected={action('variant-selected')}
    variants={variantsData}
    colorsMetadata={colorsMetadata}
    defaultColor={defaultColor}
    colors={colors}
  />
));
