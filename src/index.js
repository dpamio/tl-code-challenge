import React from 'react';
import ReactDOM from 'react-dom';
import './styles/styles.css';
import App from './pages/Product/ProductPage';
import registerServiceWorker from './registerServiceWorker';

// eslint-disable-next-line react/jsx-filename-extension
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
