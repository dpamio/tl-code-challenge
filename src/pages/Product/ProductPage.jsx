import React, { Component } from 'react';
import ImageGallery from 'react-image-gallery';
import sanitizeHtml from 'sanitize-html';
import MediaQuery from 'react-responsive';
import VariantSelector from '../../components/variant-selector/VariantSelector';
import ProductInfoHeader from '../../components/product-info-header/ProductInfoHeader';
import { BREAKPOINT_LG_LOWER_BOUNDARY, BREAKPOINT_MD_UPPER_BOUNDARY } from '../../util/responsive-constants';

const COLOR_NAME_BACKEND_FIELD_NAME = 'option1';
const BRA_SIZE_BACKEND_FIELD_NAME = 'option2';
const STOCK_BACKEND_FIELD_NAME = 'inventory_quantity';
const MIN_REQUIRED_STOCK_FOR_VARIANT = 10;

const NOT_FOUND_IN_ARRAY = -1;

const formatPrice = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 0,
  maximumFractionDigits: 2,
}).format;

/**
 * Filter function that checks if the variant stock is greater than the minimum required.
 * @param variant is the variant to check.
 */
const enoughStock = variant => (variant.stock >= MIN_REQUIRED_STOCK_FOR_VARIANT);

/**
 * expandBraSize... what an epic function name.
 *
 * Extracts the components of the bra size from the shortened "30A" to their band and cup sizes
 * ("30" and "A").
 *
 * Assumes the following shortened expression rules:
 *
 * 1. Band size is always numeric, two digit length (usually from 28 to 48, and is an even number).
 * 2. Cup size is the remaining of the shortened string. (usuallly a letter -A, B, C...- or a letter
 * and "1/2")
 *
 * These assumptions are based on https://www.thirdlove.com/pages/fit-finder#/bra_brand_and_size_questions
 *
 * @param shortName is the shortened string representation of the bra size as it comes from the
 * backend.
 * @returns {{bandSize: string, cupSize: string}} an object containing the two size components as a
 * split of strings.
 */
const expandBraSize = (shortName) => {
  const bandSize = shortName.substr(0, 2);
  const cupSize = shortName.substr(2);
  return { bandSize, cupSize };
};

/**
 * Maps the raw data coming from the backend to the data structure that better serves the UI
 * manipulation needed to satisfy UI requirements.
 *
 * @param variant {{id, price: string, COLOR_NAME_BACKEND_FIELD_NAME: string,
 * BRA_SIZE_BACKEND_FIELD_NAME:string , STOCK_BACKEND_FIELD_NAME: string }} is the raw data as
 * served by the backend API.
 * @returns {{id, price: string, colorName: string, bandSize: string, cupSize: string, stock:
 * string}} is the mapped
 * structure.
 */
const adaptVariantsBackendData = (variant) => {
  const { bandSize, cupSize } = expandBraSize(variant[BRA_SIZE_BACKEND_FIELD_NAME]);
  return {
    id: variant.id,
    price: variant.price,
    colorName: variant[COLOR_NAME_BACKEND_FIELD_NAME],
    bandSize,
    cupSize,
    braSize: variant[BRA_SIZE_BACKEND_FIELD_NAME],
    stock: variant[STOCK_BACKEND_FIELD_NAME],
  };
};

/**
 * Adapts the image metadata to the format expected by react-image-gallery.
 * @param image is the raw image metadata, as it comes from the backend.
 */
const adaptImagesBackendData = image => (
  { original: image.src1130 || image.Src1130, thumbnail: image.src150 });

// ToDo: Generalize this function and extract it to some utils/reduce.js function utility module.
/**
 * Reduces an array to a list of distinct COLOR_NAME_BACKEND_FIELD_NAME values.
 * @param allColors the accumulator (shall be [] the first time it's called).
 * @param currentVariant the current variant data as it comes from the Backend API.
 * @returns [] updated array with the added color from the current variant if not already present.
 */
const distinctColors = (allColors, currentVariant) => {
  if (allColors.indexOf(currentVariant[COLOR_NAME_BACKEND_FIELD_NAME]) === NOT_FOUND_IN_ARRAY) {
    allColors.push(currentVariant[COLOR_NAME_BACKEND_FIELD_NAME]);
  }
  return allColors;
};

const getVariantsPriceRange = (variants) => {
  const sortedPrices = variants.map(variant => (parseFloat(variant.price))).sort((a, b) => (a > b));

  if (sortedPrices[0] === sortedPrices[sortedPrices.length - 1]) {
    return formatPrice(sortedPrices[0]);
  }

  return `${formatPrice(sortedPrices[0])}-${formatPrice(sortedPrices[sortedPrices.length - 1])}`;
};

class ProductPage extends Component {
  constructor() {
    super();
    this.state = {
      productData: {},
      variantsData: [],
      colors: [],
      images: [],
      colorsMetadata: {},
    };
  }

  componentDidMount() {
    // Fetch product details.
    fetch(process.env.REACT_APP_PRODUCT_DETAILS_ENDPOINT).then(results => (results.json()
    )).then((data) => {
      const variantsData = data.product.variants.map(adaptVariantsBackendData).filter(enoughStock)
        .filter(variant => (variant.price)); // Remove variants with no price to show/sell.

      const imagesData = data.product.images.map(adaptImagesBackendData);

      // Sanitize description
      const sanitizedDescription = sanitizeHtml(data.product.body_html, {
        allowedTags: ['meta', 'p', 'ul', 'li', 'span'],
        allowedAttributes: {
          meta: ['charset'],
        },
      });
      document.title = data.product.title;
      this.setState({
        productData: data.product,
        variantsData,
        colors: data.product.variants.reduce(distinctColors, []),
        price: getVariantsPriceRange(variantsData),
        images: imagesData,
        sanitizedDescription,
      });

      // Fetch suggested default variant.
      fetch(process.env.REACT_APP_SUGGESTED_COLOR_ENDPOINT).then(results => (results.json()))
        .then(suggestedColor => (this.setState({
          defaultColor: suggestedColor.colorId,
        })
        ));
    });

    // Fetch color metadata.
    fetch(process.env.REACT_APP_COLOR_METADATA_ENDPOINT).then(results => (results.json()))
      .then(data => (this.setState({
        colorsMetadata: data,
      })
      ));
  }

  updatePrice(variants) {
    const { selectedVariant, price } = this.state;
    const exactVariantMatch = !Array.isArray(variants);
    const newPrice = exactVariantMatch ? formatPrice(variants.price)
      : getVariantsPriceRange(variants);

    if (exactVariantMatch // Full product selected
      && (selectedVariant === undefined || variants.id !== selectedVariant.id)) {
      // Either no current product selected or a different one.
      this.setState({
        selectedVariant: variants,
      });
    } else if (!exactVariantMatch && selectedVariant !== undefined) {
      this.setState({
        selectedVariant: undefined,
      });
    }
    if (newPrice !== price) {
      this.setState({
        price: newPrice,
      });
    }
  }

  addCurrentVariantToBag() {
    const { productData, selectedVariant } = this.state;
    // eslint-disable-next-line no-alert
    alert(`Added a ${productData.title} - ${selectedVariant.braSize} to the cart`);
  }

  render() {
    const {
      productData,
      variantsData,
      colors,
      colorsMetadata,
      defaultColor,
      price,
      images,
      sanitizedDescription,
      selectedVariant,
    } = this.state;
    return (
      <main className="Product">
        <section className="Product__ImageGallery">
          <MediaQuery minWidth={BREAKPOINT_LG_LOWER_BOUNDARY}>
            <ImageGallery
              items={images}
              showNav={false}
              thumbnailPosition="left"
              showFullscreenButton={false}
              showPlayButton={false}
            />
          </MediaQuery>
        </section>
        <section className="Product__Variants">
          <ProductInfoHeader productName={productData.title} price={price} />
          <MediaQuery maxWidth={BREAKPOINT_MD_UPPER_BOUNDARY}>
            <ImageGallery
              items={images}
              showNav={false}
              showThumbnails={false}
              showBullets
              showFullscreenButton={false}
              showPlayButton={false}
            />
          </MediaQuery>
          <VariantSelector
            onVariantsSelected={(variants) => { this.updatePrice(variants); }}
            variants={variantsData}
            colorsMetadata={colorsMetadata}
            defaultColor={defaultColor}
            colors={colors}
          />
          <section className="Product__CallToAction">
            <button type="button" className="Product__AddToBag" onClick={() => { this.addCurrentVariantToBag(); }} disabled={selectedVariant === undefined}>
              Add to bag
            </button>
          </section>
        </section>
        <section className="Product__Details">
          <h3 className="Product__DetailsTitle">
            Details
          </h3>
          <hr className="Product__DetailsDivider" />
          <div className="Product__DetailsHTML" dangerouslySetInnerHTML={{ __html: sanitizedDescription }} />
        </section>

      </main>
    );
  }
}

export default ProductPage;
