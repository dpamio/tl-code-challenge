module.exports = {
  product: {
    product: {
      id: 357375279141,
      title: '24/7â„¢ Classic Perfect Coverage Bra',
      body_html: "<meta charset=\"utf-8\">\n<p>A slightly fuller coverage version of the bra that started it all. The 24/7â„¢ Classic Full Coverage Braâ€™s signature cups are designed with hybrid memory foam that does double duty, providing softness inside and support outside. It forms to your unique shape and creates a smooth silhouette, no matter what youâ€™re wearing. The straps are lined with supple memory foam so they stay put and never dig in, and the ballet back design ensures your straps will truly never slip. Youâ€™ll wonder how you ever lived without it.</p>\n<!-- split -->\n<ul>\n<li><span>Available in B-H, 32-46 bands</span></li>\n<li><span>Ultra-thin hybrid foam, has memory foam on the inside to form to your body, and a supportive foam on the outside</span></li>\n<li><span>Incredibly soft micro jersey fabric, knitted for maximum durability</span></li>\n<li><span>Double layer ballet back with hidden elastic smooths your silhouette</span></li>\n<li><span>Memory foam straps that won't slip </span></li>\n<li>Pleated detail at the center front</li>\n<li><span>Gold alloy hardware adds a touch of shine</span></li>\n<li><span>Foam-padded hook &amp; eye with tagless (scratch-free!) printed label</span></li>\n<li><span>Flexible nylon-coated nickel-free wires</span></li>\n<li><span>Nylon/spandex</span></li>\n</ul>",
      images: [
        {
          src150: '//cdn.shopify.com/s/files/1/0305/6249/products/Naked1_FullCov_Product_1_05921f37-9d24-4a26-8943-d57f008a2f8a_150x.jpg?v=1528902845',
          src1130: '//cdn.shopify.com/s/files/1/0305/6249/products/Naked1_FullCov_Product_1_05921f37-9d24-4a26-8943-d57f008a2f8a.jpg?v=1528902845',
          srcMaster: '//cdn.shopify.com/s/files/1/0305/6249/products/Naked1_FullCov_Product_1_05921f37-9d24-4a26-8943-d57f008a2f8a.jpg?v=1528902845',
        },
        {
          src150: '//cdn.shopify.com/s/files/1/0305/6249/products/SoftPink_FullCov_SoftPink_SeamlessCheeky_HYB_NY_2018_03_003_150x.jpg?v=1528902814',
          Src1130: '//cdn.shopify.com/s/files/1/0305/6249/products/SoftPink_FullCov_SoftPink_SeamlessCheeky_HYB_NY_2018_03_003_1130x.jpg?v=1528902814',
          srcMaster: '//cdn.shopify.com/s/files/1/0305/6249/products/SoftPink_FullCov_SoftPink_SeamlessCheeky_HYB_NY_2018_03_003.jpg?v=1528902814',
        },
        {
          src150: '//cdn.shopify.com/s/files/1/0305/6249/products/Naked2_FullCov_Product_5_51c70785-63d1-4b3c-a808-8372299b3ab3_150x.jpg?v=1528902814',
          src1130: '//cdn.shopify.com/s/files/1/0305/6249/products/Naked2_FullCov_Product_5_51c70785-63d1-4b3c-a808-8372299b3ab3_1130x.jpg?v=1528902814',
          srcMaster: '//cdn.shopify.com/s/files/1/0305/6249/products/Naked2_FullCov_Product_5_51c70785-63d1-4b3c-a808-8372299b3ab3.jpg?v=1528902814',
        },
        {
          src150: '//cdn.shopify.com/s/files/1/0305/6249/products/SoftPink_FullCov_SoftPink_SeamlessCheeky_HYB_NY_2018_03_033_150x.jpg?v=1528902814',
          src1130: '//cdn.shopify.com/s/files/1/0305/6249/products/SoftPink_FullCov_SoftPink_SeamlessCheeky_HYB_NY_2018_03_033_1130x.jpg?v=1528902814',
          srcMaster: '//cdn.shopify.com/s/files/1/0305/6249/products/SoftPink_FullCov_SoftPink_SeamlessCheeky_HYB_NY_2018_03_033.jpg?v=1528902814',
        },
        {
          src150: '//cdn.shopify.com/s/files/1/0305/6249/products/SoftPink_FullCov_SoftPink_SeamlessCheeky_HYB_NY_2018_03_048_150x.jpg?v=1528902814',
          src1130: '//cdn.shopify.com/s/files/1/0305/6249/products/SoftPink_FullCov_SoftPink_SeamlessCheeky_HYB_NY_2018_03_048_1130x.jpg?v=1528902814',
          srcMaster: '//cdn.shopify.com/s/files/1/0305/6249/products/SoftPink_FullCov_SoftPink_SeamlessCheeky_HYB_NY_2018_03_048.jpg?v=1528902814',
        },
      ],
      variants: [
        {
          id: 6989569458233,
          price: '69.00',
          option1: 'naked-1',
          option2: '32E',
          inventory_quantity: 764,
        },
        {
          id: 6989569491001,
          price: '65.00',
          option1: 'naked-1',
          option2: '32F',
          inventory_quantity: 158,
        },
        {
          id: 4615727513637,
          price: '68.00',
          option1: 'naked-1',
          option2: '34D',
          inventory_quantity: 5,
        },
        {
          id: 4615727906853,
          price: '78.50',
          option1: 'naked-1',
          option2: '38E',
          inventory_quantity: 6,
        },
        {
          id: 6989722583097,
          option1: 'naked-2',
          option2: '32E',
          price: '68.00',
          inventory_quantity: 1109,
        },
        {
          id: 6989722615865,
          option1: 'naked-2',
          option2: '32F',
          price: '68.00',
          inventory_quantity: 1109,
        },
        {
          id: 4615861469221,
          price: '68.00',
          option1: 'naked-2',
          option2: '34D',
          inventory_quantity: 1797,
        },
        {
          id: 6989722648633,
          price: '68.00',
          option1: 'naked-2',
          option2: '34E',
          inventory_quantity: 0,
        },
        {
          id: 6989722648633,
          price: '68.00',
          option1: 'naked-2',
          option2: '34F',
          inventory_quantity: 100,
        },
        {
          id: 6989459193913,
          price: '68.00',
          option1: 'naked-3',
          option2: '32E',
          inventory_quantity: 300,
        },
        {
          id: 6989459226681,
          price: '68',
          option1: 'naked-3',
          option2: '32F',
          inventory_quantity: 320,
        },
        {
          id: 6989459292217,
          price: '68.99',
          option1: 'naked-3',
          option2: '34F',
          inventory_quantity: 264,
        },
        {
          id: 4615725842469,
          price: '70.00',
          option1: 'naked-4',
          option2: '32E',
          inventory_quantity: 214,
        },
        {
          id: 4615725908005,
          price: '68.00',
          option1: 'naked-4',
          option2: '34D',
          inventory_quantity: 133,
        },
        {
          id: 4615725973541,
          price: '120.00',
          option1: 'naked-4',
          option2: '34F',
          inventory_quantity: 891,
        },
        {
          id: 6989673398329,
          price: '68.00',
          option1: 'naked-5',
          option2: '32E(DD)',
          inventory_quantity: 98,
        },
        {
          id: 6989673431097,
          price: '68.00',
          option1: 'naked-5',
          option2: '32F',
          inventory_quantity: 98,
        },
        {
          id: 6989673463865,
          option1: 'naked-5',
          option2: '34D',
          inventory_quantity: 8,
        },
        {
          id: 6989673496633,
          price: '68.00',
          option1: 'naked-5',
          option2: '34E',
          inventory_quantity: 348,
        },
      ],
    },
  },
  colorsMetadata: {
    'naked-1': {
      name: 'Naked 1',
      color: '#ffefe5',
    },
    'naked-2': {
      name: 'Naked 2',
      color: '#dab5a1',
    },

    'naked-3': {
      name: 'Naked 3',
      color: '#d49d89',
    },

    'naked-4': {
      name: 'Naked 4',
      color: '#6a3a33',
    },
    'naked-5': {
      name: 'Naked 5',
      color: '#524346',
    },
  },
  suggestedColor: {
    colorId: 'naked-2',
  },
};
