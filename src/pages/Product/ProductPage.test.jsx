import React from 'react';
import ReactDOM from 'react-dom';
import ProductPage from './ProductPage';
import { colorsMetadata, product, suggestedColor } from './mockBackend';

it('renders without crashing', () => {
  fetch
    .once(JSON.stringify(product))
    .once(JSON.stringify(colorsMetadata))
    .once(JSON.stringify(suggestedColor));
  const div = document.createElement('div');
  ReactDOM.render(<ProductPage />, div);
  ReactDOM.unmountComponentAtNode(div);
});
