module.exports = {
  BREAKPOINT_MD_UPPER_BOUNDARY: 1023,
  BREAKPOINT_LG_LOWER_BOUNDARY: 1024,
};
