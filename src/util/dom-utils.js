module.exports = {
  /**
   * Generates a html select option object (label/value) out of the input value.
   * @param value is the source value.
   * @returns {{label: string, value: string}} is the option object.
   */
  convertToSelectOptionObject: value => ({ label: value, value }),
};
